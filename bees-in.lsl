// bees inbound

integer CHANNEL = 504;

Start() {

    llLinkParticleSystem(LINK_THIS,[
        PSYS_PART_FLAGS,
            PSYS_PART_INTERP_COLOR_MASK|
            PSYS_PART_TARGET_POS_MASK|
            PSYS_PART_EMISSIVE_MASK,
        PSYS_PART_END_ALPHA,.5,
        PSYS_PART_START_SCALE,<.1,.1,0>,
        PSYS_PART_END_SCALE,<.1,.1,.1>,
        PSYS_PART_MAX_AGE,4.0,
        PSYS_SRC_PATTERN,
            PSYS_SRC_PATTERN_ANGLE_CONE,
        PSYS_SRC_TEXTURE,(key)"7dea0717-56d2-bb25-c122-ea7817cf85a9",
        PSYS_SRC_BURST_RATE,1.5,
        PSYS_SRC_BURST_RADIUS,2.0,
        PSYS_SRC_BURST_SPEED_MIN,.1,
        PSYS_SRC_BURST_SPEED_MAX,.1,
        PSYS_SRC_ANGLE_BEGIN,1.8,
        PSYS_PART_BLEND_FUNC_SOURCE,
            PSYS_PART_BF_SOURCE_COLOR
    ]);
}

Stop() {
    llParticleSystem([]);   
}

default {
    state_entry() {
        llListen(CHANNEL, "", NULL_KEY, ""); 
        Start();
//        llSetAlpha(0.0,ALL_SIDES);
    }

    listen(integer channel, string name, key id, string message) {
 
        if (0 == llSubStringIndex(message, "start")) {
            Start();
        } else if (0 == llSubStringIndex(message, "stop")) {
            Stop();
        }
    }
}

// END //
