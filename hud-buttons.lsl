// master button - detect touch and show text
// 2 button

integer VERBOSE = FALSE;

string TEXT = "";

button(string name, vector p) {
    if (name == "reset") {
        send(501, "clear");
        send(502, "reset");
        send(503, "stop");
        send(504, "start");
        send(511, "hide");
        send(512, "hide");
    }
    else if (name == "growth") {
        // handle 3 buttons
        integer bn = (integer)(p.x * 3);
        if (bn == 0) {
            send(501, "clear");
            send(502, "shrink");
        }
        else if (bn == 1) {
            send(501, "show 3");
            send(502, "grow");
        }
        else if (bn == 2) {
            send(501, "show 2");
        }
    }
    else if (name == "snow") {
        // handle 2 buttons
        integer bn = (integer)(p.x * 2);
        if (bn == 0) {
            send(503, "stop");
        }
        else if (bn == 1) {
            send(503, "start");
        }
    }
    else if (name == "sword1") {
        // handle 2 buttons
        integer bn = (integer)(p.x * 2);
        if (bn == 0) {
            send(511, "hide");
        }
        else if (bn == 1) {
            send(511, "show");
        }
    }
    else if (name == "sword2") {
        // handle 2 buttons
        integer bn = (integer)(p.x * 2);
        if (bn == 0) {
            send(512, "hide");
        }
        else if (bn == 1) {
            send(512, "show");
        }
    }
    else if (name == "bees") {
        // handle 2 buttons
        integer bn = (integer)(p.x * 2);
        if (bn == 0) {
            send(504, "stop");
        }
        else if (bn == 1) {
            send(504, "start");
        }
    }
    else if (name == "dance") {
        // handle 2 buttons
        integer bn = (integer)(p.x * 2);
        if (bn == 0) {
            send(8, "hide");
        }
        else if (bn == 1) {
            send(8, "show");
        }
    }
}

debug(string txt) {
    if (VERBOSE) {
        llOwnerSay(txt);
    }
}

send(integer channel, string message) {
    llRegionSay(channel, message);
    debug(message);
}

// Reset HUD titles
reset() {
    integer i = llGetNumberOfPrims();
    for (; i >= 0; --i) {
        list p = llGetLinkPrimitiveParams(i, [PRIM_NAME, PRIM_DESC]);
        llSetLinkPrimitiveParamsFast(i, [
            PRIM_TEXT, llList2String(p, 1), <1,1,1>, 1
        ]);
    }
}

default {
    state_entry() {
        reset();
    }

    touch_start(integer total_number) {
        integer link = llDetectedLinkNumber(0);
        integer face = llDetectedTouchFace(0);
        vector pos = llDetectedTouchST(0);

        list p = llGetLinkPrimitiveParams(link, [PRIM_NAME, PRIM_DESC]);
        
        button(llGetLinkName(link), pos);
    }
}
