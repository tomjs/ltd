// teleport - WaS handler_blocking_fast method
//
// v3 - add sleep()
// v4 - hide object during TP
//
// Configure in object desciption:
// <src>:<dest>[:<channel>][:nohover][:debug]

// final rotation is set by this prim

// Set FALSE for no hover text
integer hover_text = TRUE;

// Set to default net channel
integer default_channel = -424242;

// Set to the 'normal' transpareny value for not-hidden state
float alpha_on = 0.3;  // 1.0;

integer DEBUG = FALSE;

list pads;

string pad_name;
string source;
string destination;
integer channel;

integer collecting = FALSE;
vector src_pos = ZERO_VECTOR;
vector dest_pos = ZERO_VECTOR;
rotation dest_rot = ZERO_ROTATION;

debug(string msg) {
    if (DEBUG) {
        llOwnerSay(msg);
    }
}

// Returns a list of the parsed description: [source, destination, channel]
// <src>:<dest>[:<channel>][:nohover][:debug]
// booleans are set directly and not returned

list parse_desc(string desc) {
        list _args = llParseString2List(desc, [ ":" ], []);
        string _src = llList2String(_args, 0);
        string _dest = llList2String(_args, 1);

        integer _chan = (integer)llList2String(_args, 2);
        if (_chan == 0) {
            _chan = default_channel;
        } else {
            if (_chan > 0)
                _chan = - _chan;
        }

        integer i = 2;
        string s = "";
        s = llList2String(_args, i);
        while (s != "") {
            debug("args: " + s);
            if (s == "nohover") {
                hover_text = FALSE;
            }
            else if (s == "debug") {
                DEBUG = TRUE;
            }
            i++;
            s = llList2String(_args, i);
        }

        return [_src, _dest, _chan];
}

bcast() {
    llRegionSay(channel, llDumpList2String([source, llGetPos(), llGetRot() ], ":"));
}

// Configure sit target for destination
set_dest(string dest) {
    src_pos = llGetPos();
    integer index = llListFindList(pads, [dest]);
    if (index > -1) {
        dest_pos = llList2Vector(pads, index+1);
        dest_rot = llList2Rot(pads, index+2);

        llSitTarget(<0,.0,1>, ZERO_ROTATION);
        llSetClickAction(CLICK_ACTION_SIT);
    }
}

//////////////////////////////////////////////////////////
// (c) Wizardry and Steamworks - 2012, License GPLv3    //
// Please see: http://www.gnu.org/licenses/gpl.html     //
// for legal details, rights of fair usage and          //
// the disclaimer and warranty conditions.              //
//////////////////////////////////////////////////////////

// Calculate next jump gate
vector nextJump(vector iPos, vector dPos, integer jumpDistance) {
    // We move 1/jumpDistance from the initial position
    // towards the final destination in the description.
    float dist = llVecDist(iPos, dPos);
    if(dist > jumpDistance) 
        return iPos + jumpDistance * (dPos-iPos) / dist;
    return nextJump(iPos, dPos, --jumpDistance);
}

default {
    on_rez(integer start_param) {
        llResetScript(); 
    }

    state_entry() {
        // Get configuration
        list desc = parse_desc(llGetObjectDesc());
        source = llList2String(desc, 0);
        destination = llList2String(desc, 1);
        channel = llList2Integer(desc, 2);

        // See who is out there
        collecting = TRUE;
        llListen(channel, "", NULL_KEY, "");
        llRegionSay(channel ,"ping");
        llSleep(0.2 + llFrand(0.3));
        llSetTimerEvent(2.0);
        bcast();
        llSetSitText("Teleport");
        if (hover_text) {
            llSetText("Teleport to " + destination, <1.0,1.0,1.0>, 1.0);
        } else {
            llSetText("", <1.0,1.0,1.0>, 1.0);
        }
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
    }

    listen(integer chan, string name, key id, string message) {
        // Only listen to our own
        key ok = llGetOwnerKey(id);
        if (ok != llGetOwner()) return;

        if (message == "ping") {
            // See who else is out there
            pads = [];
            llSetTimerEvent(2.0);
            collecting = TRUE;
            bcast();
            return;
        }

        // Process received messages
        list args = llParseString2List(message,[ ":" ], []);
        if (llGetListLength(args) != 3) return;

        // Look for our configured destination
        string name = llList2String(args, 0);
        if (name != destination)
            return;
        debug("setting up " + name);

        vector vec = (vector)llList2String(args, 1);
        rotation rot = (rotation)llList2String(args, 2);       
        integer i = llListFindList(pads, [name]);
        if (i > -1) {
            pads = llListReplaceList(pads, [name, vec, rot], i, i+2);
        } else {
            pads += [name, vec, rot];
        }

        set_dest(name);
    }

    changed(integer change) {
        if (change & CHANGED_LINK) {
            // See if someone sat down
            if (llAvatarOnSitTarget()) {
                state move;
            }
        }
        else if (change & (CHANGED_SHAPE | CHANGED_REGION)) {
            // We got resized or moved, reconfigure
            set_dest(destination);
            bcast();
            llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
            debug("reconfigured");
        }
    }

    timer() {
        if (collecting) {
            llSetTimerEvent(0.0);
            collecting = FALSE;
            return;
        }
    }
        
    moving_end() {
        set_dest(destination);
        bcast();
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
        debug("moved to " + (string)llGetPos());
    }
}

state move {
    state_entry() {
        set_dest(destination);
        llSetLinkAlpha(LINK_SET, 0.0, ALL_SIDES);
        do {
            llSetLinkPrimitiveParamsFast(LINK_THIS,
                [PRIM_POSITION, nextJump(llGetPos(), dest_pos, 10)]
            );
        } while(llVecDist(llGetPos(), dest_pos) > 1);
        if (llAvatarOnSitTarget())
            llSleep(0.25);
            llUnSit(llAvatarOnSitTarget());
        state recall;
    }
}
 
state recall {
    state_entry() {
        llSetTimerEvent(1.175494351E-38);
        llSleep(0.25);
        do {
            llSetLinkPrimitiveParamsFast(LINK_THIS,
                [PRIM_POSITION, nextJump(llGetPos(), src_pos, 10)]
            );
        } while(llVecDist(llGetPos(), src_pos) > 1);
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
        state default;
    }
}
