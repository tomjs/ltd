// Prim Resizer (shrink/expand) by Omei Qunhua
// e.g. Roll a blind from the top. (use a prim with slice end = 50%)
 
// Note: This script can rescale a prim on any or all axes
 
// Define the large and small sizes of the prim here:-
// (if the prim is sliced or path cut, it will appear to be half size on the affected dimension)
vector  gScaleLarge = <2.50000, 4.50000, 4.18800>;
vector  gScaleSmall = <1.20000, 1.50000, 3.00000>;
float   gScaleMid = 0.0;

integer gSteps  = 100;       // Number of steps in the shrink/expand process
float   gSwitch = -1.0;      // Action on first touch. +1.0 = shrink, -1.0 = expand
float   gWait   = 0.1;     // Time between steps (seconds)


// Local command channel
integer CHANNEL = 502;

// Spew debug info
integer VERBOSE = TRUE;

// Listen handles
integer hBcast = 0;
integer hChannel = 0;

debug(string txt) {
    if (VERBOSE) {
        llOwnerSay(txt);
    }
}

reset() {
    // Tear down old stuffs
//    llSetTimerEvent(0.0);
    llListenRemove(hChannel);

    // Start anew
    hChannel = llListen(CHANNEL, "", NULL_KEY, "");

    // Save half the distance to the goal
    gScaleMid = llVecDist(gScaleLarge, gScaleSmall) / 2;

    llSetScale(gScaleSmall);
    gSwitch = -1.0;
}

resize() {
    vector wscale = llGetScale();
    vector ScaleStep = (gScaleLarge - gScaleSmall) / gSteps;  // Compute the scale augment per step
    integer i;
    for ( ; i < gSteps; ++i ) {
        // It is more lag-friendly to incorporate a sleep per step
        // Rather than greatly increasing the number of steps
        llSleep(gWait);      
        llSetScale(wscale + ScaleStep * (float) i * gSwitch); 
    }
}

grow() {
    vector wscale = llGetScale();
    if (llVecDist(wscale, gScaleSmall) > gScaleMid)  {
        // We're already grown...
        return;
    }
    gSwitch = 1.0;
    resize();
}

shrink() {
    vector wscale = llGetScale();
    if (llVecDist(wscale, gScaleLarge) > gScaleMid)  {
        // We're already shrunk...
        return;
    }
    gSwitch = -1.0;
    resize();
}

toggle() {
    gSwitch *= -1;       // Switch between stretch and contract
    resize();
}

default {
    state_entry() {
        reset();
    }
 
    on_rez(integer x) {
        llResetScript();
    }
 
    touch_start(integer total_number) {
        toggle();
    }

    listen(integer channel, string name, key id, string msg) {
        string cmd = "";
        integer idx = llSubStringIndex(msg, " ");
        if (idx == -1) {
            cmd = msg;
        } else {
            cmd = llGetSubString(msg, 0, idx-1);
            msg = llDeleteSubString(msg, 0, idx);
        }
        cmd = llToLower(cmd);

        if (cmd == "reset") {
            reset();
        }
        else if (cmd == "debug") {
            VERBOSE = !VERBOSE;
            debug("Debug toggled");
        }
        else if (cmd == "tog" || cmd == "toggle") {
            toggle();
        }
        else if (cmd == "grow") {
            grow();
        }
        else if (cmd == "shrink") {
            shrink();
        }
    }

}
