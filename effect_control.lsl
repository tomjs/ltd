// effect panel - Display layered textures
// v1.0

// Define a texture/color/alpha for a stack of textures and fade them
// in and out.  Full-transparent is the default state.

// TEXTURE_BLANK, TEXTURE_TRANSPARENT
// uv: 9abe0488-e91d-eecb-7e85-32d179fd60b5
list Texture = [
    "",
    "9abe0488-e91d-eecb-7e85-32d179fd60b5",
    "0885a5e6-f8cb-b8f2-9cb3-4a4bf82c1a49",
    "2e4aa242-db9a-2c73-9941-c90de7e52519",
    TEXTURE_BLANK
];

list Color = [
    <0.0, 0.0, 0.0>,
    <1.0, 1.0, 1.0>,
    <0.5, 0.5, 0.0>,
    <1.0, 1.0, 1.0>,
    <1.0, 1.0, 1.0>
];

list Alpha = [
    0.0,
    0.5,
    0.9,
    0.9,
    0.5
];

list Visible = [
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE
];

// Alpha 'at-rest' value (aka, clear or transparent or open)
float ClearAlpha = 0.0;

// Image advance time, 0 for no auto-advance
float Time = 4.0;

// Number of transition steps
integer Steps = 50;

integer FACE = ALL_SIDES;  // 4;

// Local command channel
integer CHANNEL = 501;

// Spew debug info
integer VERBOSE = FALSE;

// When Changing <> 0, this is the link that is changing
integer Changing = 0;
integer ChangeLink = 0;

// Listen handles
integer hBcast = 0;
integer hChannel = 0;

debug(string txt) {
    if (VERBOSE) {
        llOwnerSay(txt);
    }
}

integer isvisible() {
    return (integer)llList2String(Visible, ChangeLink);
}

set_link_face(integer link, string texture, vector color, float alpha, integer face) {
    debug("set_link_face " + (string)link + ", " + texture + ", " + (string)alpha + ", " + (string)face);
    llSetLinkTexture(link, texture, face);
//    llSetLinkColor(link, color, face);
//    set_alpha(link, alpha, face);
    llSetLinkPrimitiveParamsFast(
        link, [
//        PRIM_TEXTURE, face, [texture, ],
        PRIM_COLOR, face, color, alpha
    ]);
    
}

set_alpha(integer link, float alpha, integer face) {
    llSetLinkAlpha(link, alpha, face);
}

show(integer newstate) {
    Visible = llListReplaceList(Visible, [newstate], ChangeLink, ChangeLink);
    llSetTimerEvent(Time/(float)Steps);
}

set(integer visible) {
    if (Changing == 0) {
        Changing = Steps;
        show(visible);
    }
}

toggle() {
    if (Changing == 0) {
        Changing = Steps;
        show(!isvisible());
    }
}

reset() {
    // Tear down old stuffs
    llSetTimerEvent(0.0);
    llListenRemove(hChannel);

    // Start anew
    hChannel = llListen(CHANNEL, "", NULL_KEY, "");

    integer i;
    for (i = 1; i < llGetListLength(Texture); i++) {
        set_link_face(
            i,
            llList2String(Texture, i),
            (vector)llList2String(Color, i),
            (float)llList2String(Alpha, i),
            FACE
        );
        Visible = llListReplaceList(Visible, [TRUE], i, i);
    }
    Changing = 0;
}

default {
    state_entry() {
        reset();
    }

    listen(integer channel, string name, key id, string msg) {
        string cmd = "";
        integer idx = llSubStringIndex(msg, " ");
        if (idx == -1) {
            cmd = msg;
        } else {
            cmd = llGetSubString(msg, 0, idx-1);
            msg = llDeleteSubString(msg, 0, idx);
        }
        cmd = llToLower(cmd);

        if (cmd == "reset") {
            reset();
        }
        else if (cmd == "debug") {
            VERBOSE = !VERBOSE;
            debug("Debug toggled");
        }
        else if (cmd == "tog" || cmd == "toggle") {
            ChangeLink = (integer)msg;
            toggle();
        }
        else if (cmd == "hide") {
            ChangeLink = (integer)msg;
            set(FALSE);
        }
        else if (cmd == "show") {
            ChangeLink = (integer)msg;
            set(TRUE);
        }
        else if (cmd == "clear") {
            integer i;
            for (i = 1; i < llGetListLength(Texture); i++) {
                set_link_face(
                    i,
                    llList2String(Texture, i),
                    (vector)llList2String(Color, i),
                    0.0,
                    FACE
                );
                Visible = llListReplaceList(Visible, [TRUE], i, i);
            }
        }
    }

    timer() {
        if (Changing > 0) {
            Changing--;
                float showalpha = (float)llList2String(Alpha, ChangeLink);
                float amount = (showalpha - ClearAlpha) * (float)Changing/(float)Steps;
                if (!isvisible())
                    amount = ClearAlpha + amount;
                else
                    amount = showalpha - amount;
                set_alpha(ChangeLink, amount, FACE);
            if (Changing != 0) {
                return;
            }
            llSetTimerEvent(0.0);
        }
    }

    on_rez(integer start_param) {
        llResetScript();
    }
}
